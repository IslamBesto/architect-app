package io.androidpro.architect

import io.androidpro.domain.Address
import io.androidpro.domain.Order
import java.util.Date

class OrderListItemDisplayableMapper() {
    fun map(order: Order): OrderListItemDisplayable {
        return OrderListItemDisplayable(
            orderId = order.orderId,
            customerAddress = order.customerAddress.toReadableAddress(),
            orderItems = order.orderItems.joinToString(separator = ", ") { it.itemName },
            orderStatus = order.orderStatus,
            pickupDate = Date(order.timestamp),
            restaurantAddress = order.restaurantAddress.toReadableAddress(),
            restaurantName = order.restaurantName
        )
    }

    private fun Address.toReadableAddress(): String {
        return this.address
    }
}