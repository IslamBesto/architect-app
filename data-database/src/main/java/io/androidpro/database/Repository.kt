package io.androidpro.database

import io.androidpro.domain.Address
import io.androidpro.domain.Order
import io.androidpro.domain.OrderItem
import io.androidpro.domain.OrderStatus
import io.androidpro.domain.OrdersRepository
import kotlinx.coroutines.delay
import kotlin.random.Random

/**
 * Created by jaroslawmichalik on 02/10/2023
 */
class FakeRepository: OrdersRepository {

    private val orders = List(99) { i ->
        val orderId = i.toString()
        val customerName = "Customer $i"
        val customerAddress = generateFakeAddress()
        val customerContact = "Contact $i"
        val restaurantName = "Restaurant ${Random.nextInt(1, 5)}"
        val restaurantAddress = generateFakeAddress()
        val orderItems = generateFakeOrderItems(Random.nextInt(1, 5))
        val specialInstructions = if (Random.nextBoolean()) "Special Instructions $i" else null
        val orderStatus = OrderStatus.values()[Random.nextInt(OrderStatus.values().size)]
        val timestamp = System.currentTimeMillis()

        Order(
            orderId,
            customerName,
            customerAddress,
            customerContact,
            restaurantName,
            restaurantAddress,
            orderItems,
            specialInstructions,
            orderStatus,
            timestamp
        )
    }

    override suspend fun getAll(): List<Order> {
        delay(2000)
        return orders
    }

    override suspend fun getById(id: String): Order {
        delay(300)
        return orders.first { it.orderId == id }
    }

    private fun generateFakeOrderItems(numberOfItems: Int): List<OrderItem> {
        val random = Random

        return (1..numberOfItems).map { i ->
            val itemName = "Item $i"
            val quantity = random.nextInt(1, 5)
            val price = random.nextDouble(5.0, 20.0)

            OrderItem(itemName, quantity, price)
        }
    }


    private fun generateFakeAddress(): Address {
        val latitudes = Random.nextDouble(-90.0, 90.0)
        val longitudes = Random.nextDouble(-180.0, 180.0)

        val streetNames = listOf("Maple", "Oak", "Pine", "Elm", "Cedar")
        val streetTypes = listOf("Street", "Avenue", "Boulevard", "Road", "Lane")
        val cityNames = listOf("Springfield", "Rivertown", "Laketown", "Hillview", "Greenfield")

        val address = "${Random.nextInt(1, 10000)} ${streetNames.random()} ${streetTypes.random()}, ${cityNames.random()}, " +
                "State, ${Random.nextInt(10000, 99999)}"

        return Address(latitudes, longitudes, address)
    }
}
